/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import WakerLock from './comm/WakerLock';
import BaseEvent from './BaseEvent'
import marsnapi from 'libmarsnapi.so';
import AppLogic from './app/AppLogic';
import SdtLogic from './sdt/SdtLogic';
import StnLogic from './stn/StnLogic';
import PlatformComm from './comm/PlatformComm';
import CommonError from './comm/CommonError';

class Mars{

/*    public static loadDefaultMarsLibrary(): void{
        try {
            System.loadLibrary("c++_shared");
            System.loadLibrary("marsxlog");
            System.loadLibrary("marsstn");
        }catch (e) {
            console.error("mars.Mars", "", e);
        }
    }*/

    private static hasInitialized: boolean = false;

    /**
     * APP创建时初始化平台回调 必须在onCreate方法前调用
     * @param _context
     * @param _handler
     */
    public static init(): void {
        marsnapi.AddClass("com.ohos.mars.app.AppLogic", AppLogic);
        marsnapi.AddClass("com.ohos.mars.comm.PlatformComm.C2JS", PlatformComm.C2JS);
        marsnapi.AddClass("com.ohos.mars.comm.CommonError", CommonError);
        marsnapi.AddClass("com.ohos.mars.sdt.SdtLogic", SdtLogic);
        marsnapi.AddClass("com.ohos.mars.comm.WakerLock", WakerLock);
        marsnapi.AddClass("com.ohos.mars.stn.StnLogic", StnLogic);
        Mars.hasInitialized = true;
    }

    /**
     * APP启动时首次调用onCreate前必须显示调用 init(Context _context, Handler _handler)方法 和
     * SignalTransmitNetwork设置长连接和短连接域名ip
     * @param isFirstStartup 是否首次进行mars create
     */
    public static onCreate(isFirstStartup: boolean): void {
        console.info("------onCreate------")
        if (isFirstStartup && Mars.hasInitialized) {
            BaseEvent.onCreate();
        }
        else if (!isFirstStartup) {
            BaseEvent.onCreate();
        }
        else {
            /**
             * 首次启动但未调用init 无法进行BaseEvent create
             */
            throw new Error("function MarsCore.init must be executed before Mars.onCreate when application firststartup.");
        }
    }

    /**
     *  APP退出时 销毁组件
     */
    public static onDestroy(): void {
        BaseEvent.onDestroy();
    }

}
export default Mars;
/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import observer from '@ohos.telephony.observer';
import radio from '@ohos.telephony.radio';
import wifi from '@ohos.wifi';
import { Callback } from '@ohos.base';

class NetworkSignalUtil {
  private static MIN_RSSI: number = -112;
  private static MAX_RSSI: number = 93;
  private static strength: number = 10000;
  public static readonly TAG: String = "MicroMsg.NetworkSignalUtil";

  public static InitNetworkSignalUtil(): void {
    observer.on("signalInfoChange", data => {
      console.log("signalInfoChange, data:" + JSON.stringify(data));
      if (data.length > 0) {
        console.log("signalStrength:" + data[0].signalType);
        NetworkSignalUtil.calSignalStrength(data[0]);
      }
    });
  }

  public static getNetworkSignalStrength(isWifi: boolean): number {
    return 0;
  }

  public static getGSMSignalStrength(): number {
    return NetworkSignalUtil.strength;
  }

  public static getWifiSignalStrength(callback: Callback<number>) {
    wifi.getLinkedInfo((err, data) => {
      if (err) {
        console.error('get wifi linked info error: ' + JSON.stringify(err));
        callback(0)
      } else {
        console.info("get wifi linked info: " + JSON.stringify(data));
        if (data != null && data.bssid != "") {
          let sig: number = wifi.getSignalLevel(data.rssi, data.band) * 2
          sig = (sig > 10 ? 10 : sig)
          sig = (sig < 0 ? 0 : sig)
          callback(sig * 10)
        } else {
          callback(0)
        }
      }
    })
  }

  public static calSignalStrength(sig: any): void {
    let nSig: number = 0;
    if (sig.signalType == radio.NetworkType.NETWORK_TYPE_GSM) {
      nSig = sig.signalLevel;
    }
    else if (radio.NetworkType.NETWORK_TYPE_CDMA < sig.networkType
    && sig.networkType < radio.NetworkType.NETWORK_TYPE_TDSCDMA) {
      nSig = (sig.signalLevel + 113) / 2;
    } else if (sig.signalType == radio.NetworkType.NETWORK_TYPE_LTE) {
      nSig = sig.signalLevel;
    }
    if (sig.signalType == radio.NetworkType.NETWORK_TYPE_GSM && nSig == 99) {
      this.strength = 0;
    }
    else {
      this.strength = nSig * (100 / 31);
      this.strength = (this.strength > 100 ? 100 : this.strength);
      this.strength = (this.strength < 0 ? 0 : this.strength);
    }
  }
}

export default NetworkSignalUtil;
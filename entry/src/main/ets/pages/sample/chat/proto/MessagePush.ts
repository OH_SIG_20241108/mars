/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import protobuf from '@ohos/protobufjs'
import { Protobuf } from '@ohos/protobufjs'
import MessageBuilder from './MessageBuilder'

/**
 * MessagePush proto定义
 *
 * message MessagePush {
 *     required string topic = 1;
 *     required string content = 2;
 *     required string from = 3;
 * }
 */
export default class MessagePush extends MessageBuilder {
  private topic: string = "Topic one";
  private content: string = "Hello";
  private from: string = "Client";
  private static proto: string =
    "message MessagePush {" +
    "    required string topic = 1;" +
    "    required string content = 2;" +
    "    required string from = 3;" +
    "}";

  public async mergeFrom(buffer: ArrayBuffer) {
    try {
    console.info("enter mergeFrom, buffer length:" + buffer.byteLength + ", buffer:" + new Uint8Array(buffer));
    // @ts-ignore
    let builder : Protobuf.Builder = protobuf.newBuilder();
    // @ts-ignore
    let root: Protobuf.Builder = await protobuf.loadProto(MessagePush.proto, builder, "chat.proto");
    let Response =  root.build("MessagePush");

      let decodeMsg: MessagePush = Response.decode(buffer);
      console.info("decode result:" + JSON.stringify(decodeMsg));
      this.topic = decodeMsg.topic;
      this.content = decodeMsg.content;
      this.from = decodeMsg.from;
      console.info("decode topic:" + this.topic);
      console.info("decode content:" + this.content);
      console.info("decode from:" + this.from);
    } catch (e) {
      console.info("decode exception:" + e);
    }
  }

  public getFrom(): string {
    return this.from;
  }

  public setFrom(from: string): void {
    this.from = from;
  }

  public getContent(): string {
    return this.content;
  }

  public setContent(content: string): void {
    this.content = content;
  }

  public getTopic(): string {
    return this.topic;
  }

  public setTopic(topic: string): void {
    this.topic = topic;
  }
}
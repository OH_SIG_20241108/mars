/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/
import { TableRow } from './StatisticsAdapter';
import { TaskProfile } from '@ohos/mars';

class TaskReportFragment {
  public static tableTitles: string[] = ["task", "success", "cost", "trycount", "channel"];

  public static LISTHEIGHT: number = 100;

  public table: InstanceType<typeof TableRow>[];

  public static initData(profiles: TaskProfile[]): string[] {
    let table: string[] = []
    for (let index = 0; index < profiles.length; index++) {
      const profile = profiles[index];
      let value: string = [profile.cgi.substring(profile.cgi.lastIndexOf("/") + 1),
        (profile.errCode == 0 && profile.errType == 0) ? "true " : "false",
        (profile.endTaskTime - profile.startTaskTime) + "ms",
        profile.historyNetLinkers.length.toString(),
        profile.channelSelect == 1 ? "shortlink " : "longlink "].toString();
      table.push(value)
    }
    return table;
  }
}

export default TaskReportFragment;
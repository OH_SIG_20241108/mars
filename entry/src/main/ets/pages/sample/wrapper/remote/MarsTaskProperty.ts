/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/


/**
 * Constants for Mars Task properties
 * <p></p>
 * Created by zhaoyuan on 16/2/29.
 */
export class MarsTaskProperty {
  constructor() {
  }

  public static OPTIONS_HOST: String = "host";
  public static OPTIONS_CGI_PATH: String = "cgi_path";
  public static OPTIONS_CMD_ID: String = "cmd_id";
  public static OPTIONS_CHANNEL_SHORT_SUPPORT: String = "short_support";
  public static OPTIONS_CHANNEL_LONG_SUPPORT: String = "long_support";
  public static OPTIONS_TASK_ID: String = "task_id";
}
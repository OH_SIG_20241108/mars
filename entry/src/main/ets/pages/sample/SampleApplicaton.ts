/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import BuildConfig from './BuildConfig'
import DebugMarsServiceProfile from './wrapper/service/DebugMarsServiceProfile'
import MarsServiceNative from './wrapper/service/MarsServiceNative'
import MarsServiceProfile from './wrapper/service/MarsServiceProfile'
import MarsServiceProxy from './wrapper/remote/MarsServiceProxy'
import { Xlog, Log, AppLogic } from '@ohos/mars';


class SampleApplication {
  private static readonly TAG: string = "Mars.SampleAppliscation";
  public static accountInfo: AppLogic.AccountInfo = new AppLogic.AccountInfo(2012, "anonymous");
  public static hasSetUserName: boolean = false;
  private static SampleMarsServiceProfile = class extends DebugMarsServiceProfile {
    public longLinkHost(): string {
      return "localhost";
    }
  }
  private static MarsServiceProfileFactory = class {
    public createMarsServiceProfile(): MarsServiceProfile {
      return new SampleApplication.SampleMarsServiceProfile();
    }
  };

  public static onCreate(): void {
    SampleApplication.openXlog();
    MarsServiceNative.setProfileFactory(new SampleApplication.MarsServiceProfileFactory());

    // temporary fix: ip is invalid, ip:127.0.0.1. it will be remove in future.
    // StnLogic.setDebugIP("localhost", "127.0.0.1");
    // NOTE: MarsServiceProxy is for client/caller
    // Initialize MarsServiceProxy for local client, can be moved to other place
    let marsServiceProxy = MarsServiceProxy.getInstance()
    marsServiceProxy.init('')
  }

  public static onTerminate(): void {
    Log.appenderClose();
  }

  public static openXlog(): void {

    let logPath: string = "/data/storage/el2/base/haps/entry/files/";
    let logFileName: string = "MarsSample"
    let level;
    let xlog: Xlog = new Xlog();
    if (BuildConfig.DEBUG) {
      level = Xlog.LEVEL_VERBOSE;
      xlog.setConsoleLogOpen(0, true);
    } else {
      level = Xlog.LEVEL_INFO;
      xlog.setConsoleLogOpen(0, false);
    }
    xlog.appenderOpen(level, Xlog.AppednerModeAsync, "", logPath, logFileName, 0);
    Log.setLogImp(xlog);
  }

  /**
   * 生成范围随机数
   * @Min 最小值
   * @Max 最大值
   */
  public static GetRandomNum(Min: number, Max: number): number {
    let Range = Max - Min;
    let Rand = Math.random();
    return (Min + Math.round(Rand * Range));
  }
}

export default SampleApplication;
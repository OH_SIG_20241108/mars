# Mars 操作使用说明

## 简介

Mars 是基于TCP长连接的聊天室软件；数据通信成功率、耗时、流量的展示；网络状况检测结果展示等。

## 聊天使用步骤

### 服务器连接（以测试服为例）

1. 使用DevEco Studio 3.0 打开项目源码，找到对应文件entry/src/main/ets/MainAbility/pages/com/ohos/mars/sample/wrapper/service/DebugMarsServiceProfile.ets，更改LONG_LINK_HOST为测试服务地址：106.15.92.248。

![img](image/1.png)

2. entry/src/main/ets/MainAbility/pages/com/ohos/mars/sample/wrapper/remote/AbstractTaskWrapper.ets，更改this.setHttpRequest("106.15.92.248", "/mars/sendmessage");为测试服务地址：106.15.92.248。

![img](image/2.png)

### 验证网络连接

1.  打开 3568 开发板wifi 连接，确保3568 已连接有效网络，可打开浏览器任意网页验证。电脑插上3568开发板，打开cmd窗口，执行hdc_tsd shell命令进入开发板，

![img](image/3.png)

2. 进入开发板以后验证3568与服务器是否网络畅通，执行命令：ping 106.15.92.248

![img](image/4.png)

3. 确认网络通常后，开始安装程序。

### 进入topic聊天室

1. 安装程序后进入程序首页；注意：此处需要两个 3568 板子都要安装好程序，用于互发消息。

![image](image/5.png)

2. 程序首页有三个模块，STN Discuss、Xlog Discuss、SDT Discuss,**两个板子的程序必须进入同一个模块**开始聊天，例如两个板子都进入STN Discuss 模块，双方就可以互发消息了，如下图所示：

![image](image/6.png)

3. 至此，mars 的聊天功能步骤已完成。